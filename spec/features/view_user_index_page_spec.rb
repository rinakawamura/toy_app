require 'rails_helper'

RSpec.feature "User views users page" do
    scenario "they see existing links" do
        test_user = create(:user)

        visit "users"

        expect(page).to have_text(test_user.name, test_user.email)
    end
end